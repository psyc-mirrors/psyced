#include <net.h>
#include <text.h>
#include <ht/http.h>

#define NAME "rating"
#define PRIVATE
#define SECURE
#include <place.gen>

#define RATING_TITLE "This is a demonstration of psyced's rating tool"
#define RATING_ITEMS 3
#define RATING_ITEM_HEIGHT 100
#define DEFAULT_RATING_AMOUNT 5
#define RATING_UNIFORM_PREFIX "https://www.psyced.org/rating/"

// hack, should be using w() instead
#define htfs(BLA) write("<body bgcolor='black' text='white'><center>"+ BLA +"</center");

mapping rating = ([ ]);

htget(prot, query, headers, qs, data, noprocess) {
	htnotify(query, headers, "_rating_test",
	    "[_nick_place] by [_web_on] from [_web_from] using '[_parameters]'.", 0, qs);
	sTextPath(query["layout"], query["lang"], "html");
	string j = query["juror"];
	unless (j) {
		hterror(prot, R_PAYMENTREQ, "Hello!? Who are you?");
		return 1;
	}
	mapping previous = rating[lower_case(j)];
	htok(prot);
	if (query["c001"]) {
		if (previous) {
			htfs("Replaced old evaluation by new one. Thank you, "+j);
		} else {
			htfs("Evaluation stored. Thank you, "+j);
		}
		rating[lower_case(j)] = query;
		log_file("RATING", "\n%O from %O using %O got %O\n",
			ME, query_ip_name(), headers["user-agent"], query);
		return 1;
	}
	w("_PAGES_start_group_rating", 0,
	    ([ "_title_page" : htquote(RATING_TITLE),
//	       "_parameters" : query["parameters"] || qs,
	       "_uniform_logo" : HT_LOGO,
	       "_nick_juror" : j || query_ip_number(),
	       "_amount_height_item" : RATING_ITEM_HEIGHT,
	       "_nick_place" : MYNICK ]) );
	// printf("%O vs %O\n", query, headers);
	for (int i=1; i <= RATING_ITEMS; i++) {
		string ii=sprintf("%03d", i);
		w("_HTML_item_rating", 0, ([
		   "_index_item" : ii,
		   "_uniform_item" : RATING_UNIFORM_PREFIX +ii,
		   "_amount_rating_item" : previous? previous["r"+ii]: DEFAULT_RATING_AMOUNT,
		   "_comment_item" : previous? previous["c"+ii] || "": "",
		   "_nick_place" : MYNICK ]) );
	}
	w("_PAGES_end_group_rating", 0, ([
	       "_comment_notes" : previous? previous["notes"] || "": "",
	       "_nick_place" : MYNICK ]) );
	return 1;

}

